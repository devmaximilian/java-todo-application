package se.wendel.maximilian.todo;

import java.sql.ResultSet;

public class Todo implements ThingToDo, Collectable, Model {

    public int id;
    public String title;
    public String description;
    public Priority priority;
    public Status status;
    public int due;
    public boolean archived;
    private String table = "todo";

    @Override
    public String[] properties() {
        return new String[] {"id", "title", "description", "priority", "status", "due", "archived"};
    }

    public Todo(int id, String title, String description, Priority priority, Status status, int due, boolean archived) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.status = status;
        this.due = due;
        this.archived = archived;
        System.out.println("Initialized: " + this.toString());
    }

    @Override
    public String toString() {
        return "Todo" + "\n\tId: " + this.id + "\n\tTitle: " + this.title + "\n\tDescription: "+this.description+ "\n\tPriority: " + this.priority+ "\n\tStatus: " + this.status+ "\n\tDue: " + this.due+ "\n\tArchived: " + this.archived;
    }

    @Override
    public String slug() {
        return "todo";
    }

    @Override
    public boolean setTitle(String value) {
        return false;
    }

    @Override
    public boolean setDescription(String value) {
        return false;
    }

    @Override
    public boolean setDue(Integer value) {
        return false;
    }

    @Override
    public boolean setArchived(Boolean value) {
        Query q = new Query();
        q.table("todo");
        q.update("archived", value.toString());
        q.where("id="+this.id);

        SQLiteDatabase db = new SQLiteDatabase("./todo.db");

        return db.run(q.sql());
    }

    @Override
    public boolean setPriority(Priority priority) {
        return false;
    }

    @Override
    public boolean setStatus(Status status) {
        return false;
    }

    @Override
    public boolean save() {
        if (this.id != 0) {
            // Insert
            SQLiteDatabase db = new SQLiteDatabase("./todo.db");

            return db.write(this);
        } else {
            // Update
            return false;
        }
    }

    @Override
    public boolean load() {
        return false;
    }
}
