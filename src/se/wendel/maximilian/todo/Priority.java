package se.wendel.maximilian.todo;

public enum Priority {
    UNKNOWN,
    HIGH,
    MEDIUM,
    LOW;

    /**
     * Get integer value for enum
     * @param priority
     * @return
     */
    public static int value(Priority priority) {
        switch (priority) {
            case HIGH:
                return 1;
            case MEDIUM:
                return 2;
            case LOW:
                return 3;
            default:
                return 0;
        }
    }
}