package se.wendel.maximilian.todo;

import java.lang.*;

public interface Database {

    public Collectable[] read();

    public Boolean write(Collectable item);

    public boolean update();

    public boolean delete();

}
