package se.wendel.maximilian.todo;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.geometry.Pos;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class TodoListItem extends GridPane {

    private Todo todo;
    public TodoApplication app;

    public TodoListItem(Todo todo, TodoApplication app) {
        super();
        this.app = app;
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(10, 10, 10, 10));
        this.todo = todo;
        this.render();
    }

    public void render() {
        Text textNode = new Text(todo.title);
        this.add(textNode, 0, 0);

        Button delete = new Button("Delete");
        this.add(delete, 1, 0);
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                todo.setArchived(true);
                app.renderTodoList();
            }
        });

        Button archive = new Button("Archive");
        this.add(archive, 2, 0);
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                todo.setArchived(true);
            }
        });
    }
}
