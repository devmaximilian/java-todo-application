package se.wendel.maximilian.todo;

import java.lang.*;

public interface Model {

    /**
     * A models properties that are stored in a database
     * @return
     */
    public String[] properties();

    /**
     * Model slug, table name
     * @return
     */
    public String slug();

}
