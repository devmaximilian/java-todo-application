package se.wendel.maximilian.todo;

import java.sql.SQLException;

public class TodoCollection implements Collection {

    private Todo[] items;

    @Override
    public void save() {

    }

    @Override
    public void load() {
        SQLiteDatabase db = new SQLiteDatabase("./todo.db");
        Todo[] records = db.read();
        this.items = records;
    }

    public void reload() {
        this.load();
    }

    public Todo[] all() {
        return this.items;
    }

    public int length() { return this.items.length; }

    public Todo get(int id) {
        return this.items[id];
    }
}
