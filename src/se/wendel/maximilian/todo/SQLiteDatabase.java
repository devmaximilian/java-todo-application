package se.wendel.maximilian.todo;

import java.sql.*;

public class SQLiteDatabase implements Database {

    private Connection dc; // Database connection

    private void status(String text) {
        System.out.println("[DB]: "+text);
    }

    /**
     * Connect to database
     * @return
     */
    private Connection connect(String path) {
        this.status("Attempting to open "+path);
        Connection connection = null;

        try {
            this.status("Setting path");
            String url = "jdbc:sqlite:"+path;
            this.status("Getting connection through DriverManager...");
            connection = DriverManager.getConnection(url);
            this.status("Connection established.");
            return connection;
        } catch (SQLException e) {
            this.status("Fatal error hit, printing error to log...");
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Create database
     * @return
     */
    public boolean createDatabase(String path) {
        String sql = "CREATE TABLE todo(id INTEGER PRIMARY KEY ASC, title STRING NOT NULL, description TEXT, status INTEGER(3) DEFAULT 0, priority INTEGER(3) DEFAULT 0, due INTEGER DEFAULT 0, archived BOOLEAN DEFAULT false);";

        try (Connection conn = DriverManager.getConnection(path);
             Statement statement = conn.createStatement()) {
            statement.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    @Override
    public Todo[] read() {
        String sql = "SELECT * FROM 'todo'";
        Todo[] items = {};

        System.out.println(sql);

        try (Connection conn = connect("todo.db");
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            int i = 0;
            // loop through the result set
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                String description = rs.getString("description");
                Priority priority = Priority.values()[rs.getInt("priority")];
                Status status = Status.values()[rs.getInt("status")];
                int due = rs.getInt("due");
                boolean archived = rs.getBoolean("archived");
                Todo[] newItems = new Todo[i+1];

                for (int j = 0; j < items.length; j++) {
                    newItems[j] = items[j];
                }

                items = newItems;
                items[i] = new Todo(id, title, description, priority, status, due, archived);
                i++;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return items;
    }

    @Override
    public Boolean write(Collectable item) {

        Todo todo = (Todo)item;

        String sql = "INSERT INTO todo(title, description, priority) VALUES(?,?,?)";
        try (Connection conn = connect("todo.db");
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, todo.title);
            pstmt.setString(2, todo.description);
            pstmt.setInt(3, Priority.value(todo.priority));
            pstmt.executeUpdate();
            return true;
        } catch(SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public Boolean run(String query) {
        String sql = query;
        System.out.println(sql);
        try {
            Connection conn = connect("todo.db");
            Statement statement = conn.createStatement();
            statement.executeQuery(sql);
            return true;
        } catch(SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean update() {
        return false;
    }

    @Override
    public boolean delete() {
        return false;
    }

    public SQLiteDatabase(String path) {
        //dc = connect(path);
    }

}
