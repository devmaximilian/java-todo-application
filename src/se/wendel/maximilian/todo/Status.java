package se.wendel.maximilian.todo;

public enum Status {
    UNKNOWN,
    TODO,
    DOING,
    DONE;

    /**
     * Get integer value for enum
     * @param status
     * @return
     */
    public int value(Status status) {
        switch (status) {
            case TODO:
                return 1;
            case DOING:
                return 2;
            case DONE:
                return 3;
            default:
                return 0;
        }
    }
}