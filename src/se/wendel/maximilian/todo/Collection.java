package se.wendel.maximilian.todo;

import java.lang.*;

public interface Collection {

    public void save();

    public void load();

}
