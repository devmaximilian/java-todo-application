package se.wendel.maximilian.todo;

import java.lang.*;

public interface ThingToDo {

    /**
     * Set property to string value
     * @param value
     * @return
     */
    public boolean setTitle(String value);

    /**
     * Set property to string value
     * @param value
     * @return
     */
    public boolean setDescription(String value);

    /**
     * Set property to integer value
     * @param value
     * @return
     */
    public boolean setDue(Integer value);

    /**
     * Set property to boolean value
     * @param value
     * @return
     */
    public boolean setArchived(Boolean value);

    /**
     * Set priority
     * @param priority
     * @return
     */
    public boolean setPriority(Priority priority);

    /**
     * Set status
     * @param status
     * @return
     */
    public boolean setStatus(Status status);

    /**
     * Save to database
     * TODO: Implement database functionality
     * @return
     */
    public boolean save();

    /**
     * Load from database
     * TODO: Implement database functionality
     * @return
     */
    public boolean load();

}
