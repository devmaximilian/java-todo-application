package se.wendel.maximilian.todo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.ScrollPane;

public class TodoApplication extends Application {

    private TodoCollection todoList;
    private Stage stage;
    private GridPane scene;
    private GridPane todoListScene = new GridPane();

    /**
     * Application entry point
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    private void fetch() {
        this.todoList = new TodoCollection();
        this.todoList.load();
    }

    public void renderTodoList() {
        this.fetch();
        this.todoListScene.getChildren().removeAll();
        for (int i = 0; i < this.todoList.length(); i++) {
            if (!this.todoList.all()[i].archived) {
                todoListScene.add(new TodoListItem(this.todoList.get(i), this), 0, i);
            }
        }
    }

    private void renderToolbar() {
        GridPane toolbar = new GridPane();

        Button addButton = new Button("+ add item");
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Stage dialog = new Stage();
                dialog.initModality(Modality.APPLICATION_MODAL);
                dialog.initOwner(stage);
                VBox dialogVbox = new VBox(20);
                dialogVbox.getChildren().add(new Text("Add new Todo item"));

                // Name
                Label nameLabel = new Label("Name:");
                TextField nameField = new TextField();
                HBox nameBox = new HBox();
                nameBox.getChildren().addAll(nameLabel, nameField);
                nameBox.setSpacing(10);
                dialogVbox.getChildren().add(nameBox);

                // Description
                Label descriptionLabel = new Label("Description:");
                TextField descriptionField = new TextField();
                HBox descriptionBox = new HBox();
                descriptionBox.getChildren().addAll(descriptionLabel, descriptionField);
                descriptionBox.setSpacing(10);
                dialogVbox.getChildren().add(descriptionBox);

                HBox buttonBox = new HBox();

                // Cancel button
                Button cancelButton = new Button("Cancel");
                cancelButton.setOnAction(new EventHandler<ActionEvent>() {
                      @Override

                      public void handle(ActionEvent e) {
                          dialog.close();
                      }
                });

                buttonBox.getChildren().add(cancelButton);

                // Create button
                Button createButton = new Button("Create");
                createButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override

                    public void handle(ActionEvent e) {
                        Todo item = new Todo(0, nameField.getText(), descriptionField.getText(), Priority.LOW, Status.UNKNOWN, 0, false);
                        System.out.println(item.toString());
                        if (item.save()) {
                            System.out.println("Saved item successfully!");
                        } else {
                            System.out.println("Failed to save item.");
                        }
                        todoList.reload();
                        renderTodoList();
                        dialog.close();
                    }
                });

                buttonBox.getChildren().add(createButton);

                dialogVbox.getChildren().add(buttonBox);
                Scene dialogScene = new Scene(dialogVbox, 300, 200);
                dialog.setScene(dialogScene);
                dialog.show();
            }
        });

        toolbar.add(addButton, 0 ,0);
        this.scene.add(toolbar, 0, 0);
    }

    /**
     * Application root view
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        this.stage = primaryStage;

        this.stage.setTitle("Todo Application");

        // Set up layout
        this.scene = new GridPane();
        ScrollPane scrollPane = new ScrollPane(this.scene);
        this.stage.setScene(new Scene(scrollPane, 375, 375));

        // Begin render
        this.stage.show();

        // Add todolist scene
        this.scene.add(todoListScene, 0, 1);

        // Render todolist
        this.renderTodoList();

        // Render toolbar
        this.renderToolbar();
    }
}
