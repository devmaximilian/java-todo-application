package se.wendel.maximilian.todo;

public class Query {
    private String table = "";
    private String operation = "";
    private String[] values = {};
    private String[] columns = {};
    private String condition = "";

    // TODO: Fix potential issue where number of columns don't match number of values
    private String makeUpdateString() {
        String update = "";

        for (int i = 0; i < this.columns.length; i++) {
            update += this.columns[i]+"="+this.values[i];

            if (i != this.columns.length-1) {
                update += ", ";
            }
        }

        return update;
    }

    // Array to comma separated string
    private String atcss(String[] items) {
        String css = "";

        for (int i = 0; i < items.length; i++) {
            css += items[i];

            if (i != items.length-1) {
                css += ", ";
            }
        }

        return css;
    }

    private String build() {
        switch (operation) {
            case "INSERT":
                return "INSERT INTO "+this.table+"("+atcss(this.columns)+")"+" VALUES "+atcss(this.values);

            case "UPDATE":
                return "UPDATE '"+this.table+"' SET "+makeUpdateString()+" WHERE "+this.condition;

            case "DELETE":
                return "DELETE "+";";

            case "SELECT":
                return "SELECT "+(this.condition.length() > 0 ? this.condition : "*")+" FROM '"+this.table+"'";

            default:
                return "";

        }
    }

    public void where(String condition) {
        this.condition = condition;
    }

    public void table(String table) {
        this.table = table;
    }

    public void insert(String[] columns, String[] values) {
        this.columns = columns;
        this.values = values;
    }

    public void select(String where) {
        this.condition = where;
    }

    public void update(String[] columns, String[] values) {
        this.columns = columns;
        this.values = values;
    }

    public void update(String column, String value) {
        this.operation = "UPDATE";
        this.columns = new String[] { column };
        this.values = new String[] { value };
    }

    public String sql() {
        return this.build();
    }
}
