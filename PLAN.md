# Hur en grupp på fyra personer skulle kunna utveckla en "att göra"-applikation i Java

Detta dokument beskriver hur en grupp på fyra personer skulle kunna utveckla en "att göra"-applikation i Java.

## Allmänt

* SCRUM skall användas så gott det går
* Eftersom att en PO inte finns skall gruppen anta att applikationen byggs för internt använande
* Förutsatt att tid finns, bör tester skrivas före kod (red, green, refactor)
* Parprogrammering när detta är applicerbart, undantaget är buggfixar (dock skall dessa granskas av annan inom gruppen innan de slås ihop med master)
* En branch per Sprint & Feature

### Innan kod skrivs

- Skapa en pappersskiss
- Skriva användartester
- Tvinga tredjepart att "testa" skissen
- Utvärdera och modifiera skissen utefter Nilsens 10 heuristics

### Delmål 1

Ett paket som kan köras.

### Delmål 2

Ett paket som visar något grafiskt

### Delmål 3

Visa en lista på saker att göra

### Delmål 4

Lägg till saker i listan

### Delmål 5

Ta bort saker från listan

### Delmål 6

Redigera saker i listan

### Delmål 7

Spara data i SQL databas
